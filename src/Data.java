
//1 Derive conclusion statements based on answers - Due
//2 Record Main answers - Done
//3 Record sub answers - Due

import java.io.IOException;
import java.util.Scanner;


public class Data {

    private  static char[] options = {'A','a','B','b','C','c','D','d'};
    public   static char[] mainAnswers = new char[5];
    public   static char[] subAnswers = new char[2];
    private  static int numberOfQuesDone;
    public static Scanner answer = new Scanner(System.in);
    public static Boolean takeInput = true;

    public Data(){
        numberOfQuesDone = 0;
    }

    public static int getNumberOfQuesDone(){
        return numberOfQuesDone;
    }

    public  static boolean validateAnswer(char response, int responseNumber) {
        if(responseNumber < 0 ){
            throw new IllegalStateException();
        }
        for (char x : options){
            if (response == x){
                if(!(Question.getisSubQues())){
                    mainAnswers[responseNumber] = response;
                    numberOfQuesDone ++;
                }
                else {
                    subAnswers[responseNumber] = response;
                    Question.setisSubQues(false);
                    numberOfQuesDone ++;
                }

                if(responseNumber == 2 || responseNumber == 3){
                    if (response == 'A'){//just say this response leads to sub ques
                        Question.setisSubQues(true);
                    }
                }
//               System.out.println("Valid Input");
                return true;
            }
        }
        return false;
    }

    public static char submitAnswer(boolean takeInput){
        if(takeInput) {
            return answer.next().charAt(0);
        }
        else{
            return 'A'; //valid input for unit test
        }
    }

    public static void takeAnswers(int answerNumber) {
        if (answerNumber < 0) {
            throw new IllegalStateException();
        }
        while (!(validateAnswer(submitAnswer(takeInput), answerNumber))) {
            Question.inValidInput();
        }
    }

}
