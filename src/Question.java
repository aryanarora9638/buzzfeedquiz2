
//1 Read text file for ques - Done
//2 serve Main Ques - Done
//3 serve sub Ques - Done

import java.io.IOException;
import java.nio.file.FileSystems;
import java.util.Scanner;
import java.nio.file.Path;

public class Question {

    private static int mainQuesNumber = 0;
    private static boolean isSubQue = false;
    private static int subQuesNumber = 0;
    private static Path pathOfTextfile = FileSystems.getDefault().getPath("quizQues.txt");

    public Question(){
        this.mainQuesNumber = 0;
        this.subQuesNumber = 0;
        this.isSubQue = false;
        this.pathOfTextfile = FileSystems.getDefault().getPath("quizQues.txt");
    }

    public  static int getMainQuesNumber(){
        return mainQuesNumber;
    }
    public  static int getSubQuesNumber(){
        return subQuesNumber;
    }
    public  static boolean getisSubQues(){
        return isSubQue;
    }
    public  static Path getPathOfTextfile() {
        return pathOfTextfile;
    }

    public static Path getPath(){
        return pathOfTextfile;
    }

    public static void setMainQuesNumber(int newMainQuesNumber){
        mainQuesNumber = newMainQuesNumber;
    }
    public static void setSubQuesNumber(int newSubQuesNumber){
        subQuesNumber = newSubQuesNumber;
    }
    public static void setisSubQues(boolean ifSubQues){
        isSubQue = ifSubQues;
    }

    public static boolean readTextFile(Scanner reader, String startFrom, String endFrom){

        while (!(reader.hasNext(endFrom))){
            if(reader.hasNext(startFrom)){ //To Avoid the first line of ques
                reader.nextLine();
            }
            else {
                System.out.println(reader.nextLine());
            }
        }
        System.out.println("NOw will take answer!!");
        if(!getisSubQues()){
            Data.takeAnswers(getMainQuesNumber());
            reader.next();
            setMainQuesNumber(getMainQuesNumber() + 1);
        }
        else{
            Data.takeAnswers(getSubQuesNumber());
            reader.next();
            setSubQuesNumber(getSubQuesNumber() + 1);
        }
        return true;

    }

    public static boolean readSubQues() throws IOException {

        Scanner readerSubQues = new Scanner(getPathOfTextfile());
        //Taking scanner to sub ques line

        while(!(readerSubQues.hasNext("<StartofSubQuestion>")) ){
            readerSubQues.nextLine();
        }
        if(getSubQuesNumber() == 1){
            readerSubQues.next();
            while(!(readerSubQues.hasNext("<StartofSubQuestion>")) ){
                readerSubQues.nextLine();
            }
        }
        readTextFile(readerSubQues,"<StartofSubQuestion>" ,"<EndofSubQuestion>");
        return true;
    }

    public static Boolean readMainQues() throws IOException {

        Scanner readerMainQues;
        readerMainQues = new Scanner(getPathOfTextfile());
        while (readerMainQues.hasNext() && !(readerMainQues.hasNext("<StartofSubQuestion>"))) {
            if(!getisSubQues()){//isSubQues = false
                readTextFile(readerMainQues, "<StartofQuestion>" , "<EndofQuestion>");
            }
            else{//isSubQues = true
                readSubQues();
            }
        }
        return true;
    }

    public static String inValidInput(){
        System.out.println("Please enter a valid input");
        return "Please enter a valid input";
    }

    public static boolean getConclusion() throws IOException {
        Conclusion.makeAnswerToText(Data.mainAnswers, Data.subAnswers);
        return true;
    }


}
