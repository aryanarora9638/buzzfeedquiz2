import java.io.IOException;
import java.nio.file.FileSystems;
import java.nio.file.Path;
import java.util.Scanner;

public class Conclusion {

    public static Path path;

    public static boolean makeAnswerToText(char [] mainAns , char [] subAns) throws IOException {
        if(mainAns[4] == 'a' || mainAns[4] == 'A'){
            path = FileSystems.getDefault().getPath("EarlyBird.txt");
        }
        if(mainAns[4] == 'b' || mainAns[4] == 'B'){
            path = FileSystems.getDefault().getPath("NightOwls.txt");
        }
        if(mainAns[4] == 'c' || mainAns[4] == 'C' || mainAns[4] == 'd' || mainAns[4] == 'D'){
            path = FileSystems.getDefault().getPath("BothEarlyAndNight.txt");
        }

        Scanner reader = new Scanner(path);
        while(reader.hasNext()){
            System.out.println(reader.nextLine());
        }
        return true;
    }
}
