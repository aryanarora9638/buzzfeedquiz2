import java.io.IOException;

public class BuzzFeedQuiz {

    public static void main(String[] args) throws IOException {
        //Objective - Reading text file
        //Secondary Objective - Storing answers/responses (called within the Question class itself)
        Question.readMainQues();
        Question.getConclusion();
    }
}

