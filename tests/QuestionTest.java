import org.junit.Test;

import java.io.IOException;
import java.nio.file.FileSystems;
import java.util.Scanner;

import static org.junit.Assert.*;

public class QuestionTest {

    private Question test = new Question();

    @Test
    public void getMainQuesNumber() {
        assertEquals(0, test.getMainQuesNumber());
    }

    @Test
    public void getSubQuesNumber() {
        assertEquals(0,test.getSubQuesNumber());
    }

    @Test
    public void getisSubQues() {
        assertFalse(test.getisSubQues());
    }

    @Test
    public void getPathOfTextfile() {
        assertEquals(FileSystems.getDefault().getPath("quizQues.txt"), test.getPathOfTextfile());
    }

    @Test
    public void getPath(){
        assertEquals(FileSystems.getDefault().getPath("quizQues.txt"), test.getPath());
    }

    @Test
    public void setMainQuesNumber() {
        test.setMainQuesNumber(10);
        assertEquals(10, test.getMainQuesNumber());
    }

    @Test
    public void setSubQuesNumber() {
        test.setSubQuesNumber(10);
        assertEquals(10,test.getSubQuesNumber());
    }

    @Test
    public void setisSubQues() {
        assertFalse(test.getisSubQues());
        test.setisSubQues(true);
        assertTrue(test.getisSubQues());
    }

    @Test
    public void readTextFile() throws IOException {
        Scanner reader = new Scanner(test.getPath());
        test.setisSubQues(false);
        test.setMainQuesNumber(1);
        test.setSubQuesNumber(1);
        Data.takeInput = false;
        assertTrue(test.readTextFile(reader,"<StartofQuestion>" , "<EndofQuestion>"));
        test.setisSubQues(true);
        assertTrue(test.readTextFile(reader,"<StartofQuestion>" , "<EndofQuestion>"));
    }

    @Test
    public void readSubQues() throws IOException {
        Data.takeInput = false;
        assertTrue(test.readSubQues());
    }

    @Test
    public void readMainQues() throws IOException {
        Data.takeInput = false;
        assertTrue(test.readMainQues());
    }

    @Test
    public void inValidInput() {
        String invalid = Question.inValidInput();
        assertEquals("Please enter a valid input" , invalid);
    }

    @Test
    public void getConcludingStatement() throws IOException {
        Data.mainAnswers[4] = 'A';
        assertTrue(Question.getConclusion());
        Data.mainAnswers[4] = 'B';
        assertTrue(Question.getConclusion());
    }
}