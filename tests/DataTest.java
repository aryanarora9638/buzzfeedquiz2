import org.junit.Test;

import java.util.Scanner;

import static org.junit.Assert.*;

public class DataTest {
    private Data test = new Data();

    @Test
    public void getNumberOfQuesDone() {
        assertEquals(0,test.getNumberOfQuesDone());
    }

    @Test
    public void mainQuesDataStorage() {
        Question.setisSubQues(false);
        assertFalse(test.validateAnswer('G',1));
        assertTrue(test.validateAnswer('A',1));
    }

    @Test
    public void subQuesTriggered(){
        assertTrue(test.validateAnswer('A',2));
    }

    @Test
    public void subQuesDataStorage(){
        Question.setisSubQues(true);
        assertTrue(test.validateAnswer('A',1));
    }

    @Test (expected = IllegalStateException.class)
    public void invalidResponseNumber(){
        test.validateAnswer('A',-1);
    }

    @Test
    public void validationFails(){
        assertFalse(test.validateAnswer('k',1));
    }

    @Test (expected = IllegalStateException.class)
    public void invalidAnswerNumber() {
        test.takeAnswers(-1);
    }

    @Test
    public void invalidResponseByUser(){
        Data.takeInput = false;
    }
}