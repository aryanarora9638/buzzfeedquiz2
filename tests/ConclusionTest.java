import org.junit.Test;

import java.io.IOException;

import static org.junit.Assert.*;

public class ConclusionTest {

    @Test
    public void makeAnswerToText() throws IOException {
        Data.mainAnswers[4] = 'A';
        assertTrue(Conclusion.makeAnswerToText(Data.mainAnswers, Data.subAnswers));
        Data.mainAnswers[4] = 'B';
        assertTrue(Conclusion.makeAnswerToText(Data.mainAnswers, Data.subAnswers));
        Data.mainAnswers[4] = 'C';
        assertTrue(Conclusion.makeAnswerToText(Data.mainAnswers, Data.subAnswers));
        Data.mainAnswers[4] = 'D';
        assertTrue(Conclusion.makeAnswerToText(Data.mainAnswers, Data.subAnswers));

    }
}